import React, { useState } from 'react';

import PropTypes from 'prop-types';

import {
  Stack, Divider, Snackbar, Alert,
} from '@mui/material';

import IssueOptions from './IssueOptions';
import IssueHeader from './IssueHeader';

const Issue = ({ content, setContent, focusEditor }) => {
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');
  const [appState, setAppState] = useState('writing');
  const [options, setOptions] = useState([]);
  const [tags, setTags] = useState([]);
  const [openSnackbar, setOpenSnackbar] = useState(false);
  const [snackbarValue, setSnackbarValue] = useState();

  const onOptionTextChange = (optionKey, value) => {
    setOptions({
      ...options,
      [optionKey]: {
        ...options[optionKey],
        text: value,
      },
    });
  };

  const onCopyOver = (e, optionKey) => {
    setContent(`
    ${content || ''}${content && '<br>'}
    <b>${title}:</b> ${options[optionKey].text}`);
    focusEditor();
  };

  const addKeyword = (keyword, from) => {
    const addition = description.length ? `, ${keyword}` : keyword;
    setDescription(`${description}${addition}`);
    setTags(tags.filter((tag) => tag !== keyword));
    if (from === 'keyword') {
      setSnackbarValue(keyword);
      setOpenSnackbar(true);
    }
  };

  return (
    <Stack marginLeft="30px" alignItems="flex-start">
      <IssueHeader
        title={title}
        setTitle={setTitle}
        description={description}
        setDescription={setDescription}
        appState={appState}
        setAppState={setAppState}
        setOptions={setOptions}
        addKeyword={addKeyword}
        tags={tags}
        setTags={setTags}
      />
      <Divider flexItem style={{ margin: '15px 0' }} />
      {options && (
        <IssueOptions
          options={options}
          onOptionTextChange={onOptionTextChange}
          onCopyOver={onCopyOver}
          addKeyword={addKeyword}
        />
      )}
      <Snackbar
        open={openSnackbar}
        autoHideDuration={2500}
        onClose={() => setOpenSnackbar(false)}
      >
        <Alert severity="success" sx={{ width: '100%' }}>
          {`${snackbarValue} added to description`}
        </Alert>
      </Snackbar>
    </Stack>
  );
};

Issue.propTypes = {
  content: PropTypes.string.isRequired,
  setContent: PropTypes.func.isRequired,
  focusEditor: PropTypes.func.isRequired,
};

export default Issue;
