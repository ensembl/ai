import React, { useState, useEffect, useRef } from 'react';

import { useNavigate } from 'react-router-dom';

import StyledFirebaseAuth from 'react-firebaseui/StyledFirebaseAuth';

import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import { collection, addDoc } from 'firebase/firestore';

import {
  Alert, Box, Button, Stack, TextField,
} from '@mui/material';

import { auth, db } from './firebase-config';

const Login = () => {
  const navigate = useNavigate();
  const [requested, setRequested] = useState(false);
  const emailRef = useRef();

  useEffect(() => {
    const unregisterAuthObserver = auth.onAuthStateChanged((user) => {
      if (!user) {
        navigate('/login');
      }
      if (user) {
        navigate('/');
      }
    });
    return () => unregisterAuthObserver();
  }, []);

  const uiConfig = {
    signInFlow: 'popup',
    signInSuccessUrl: '/',
    signInOptions: [
      firebase.auth.GoogleAuthProvider.PROVIDER_ID,
    ],
    privacyPolicyUrl: 'https://ensembl.ai/privacy-policy',
    tosUrl: 'https://ensembl.ai/terms-of-service',
  };

  const onEmailChange = (e) => {
    emailRef.current = e.target.value;
  };

  const onRequestAccess = async () => {
    await addDoc(
      collection(db, 'access'),
      {
        email: emailRef.current,
      },
    );
    setRequested(true);
  };

  return (
    <Stack marginTop="50px" justifyContent="center" alignItems="center">
      <h1 style={{
        fontSize: '2rem',
      }}
      >
        Ensembl AI beta
      </h1>
      <StyledFirebaseAuth uiConfig={uiConfig} firebaseAuth={auth} />

      <Box style={{ marginTop: '100px' }}>
        {!requested
          && (
            <Stack justifyContent="center" alignItems="center">
              <h3>Request access</h3>
              <p>Provide a Google-based work email address or Gmail email address</p>
              <Box marginTop="10px" display="flex" alignItems="center">
                <TextField
                  size="small"
                  type="email"
                  placeholder="Email address"
                  onChange={onEmailChange}
                />
                <Button
                  onClick={onRequestAccess}
                >
                  Request access
                </Button>
              </Box>
            </Stack>

          )}
        {requested && (
          <Alert>Thank you for your submission, we will reach out shortly!</Alert>
        )}
      </Box>
    </Stack>
  );
};

export default Login;
