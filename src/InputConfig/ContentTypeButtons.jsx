import React from 'react';

import PropTypes from 'prop-types';

import {
  Box, Stack, FormControl, FormLabel, ToggleButtonGroup, ToggleButton, Tooltip,
} from '@mui/material';

import HelpOutlineIcon from '@mui/icons-material/HelpOutline';

import { contentTypeTooltip, displayData } from '../utils';

const ContentTypeButtons = ({ contentType, setContentType }) => (
  <Box>
    <FormControl>
      <FormLabel id="content-type">What do you want to write about?</FormLabel>
      <Stack direction="row" alignItems="center">
        <ToggleButtonGroup
          value={contentType}
          exclusive
          onChange={(e) => setContentType(e.target.value)}
        >
          {
            Object.values(displayData)
              .map((data) => (
                <ToggleButton
                  key={data.value}
                  value={data.value}
                  size="small"
                  color="primary"
                  style={{
                    fontSize: '0.7rem', padding: '7px', textTransform: 'none', width: '7rem',
                  }}
                >
                  {data.label}
                </ToggleButton>
              ))
          }

        </ToggleButtonGroup>
        <Tooltip
          placement="right"
          title={contentTypeTooltip}
          componentsProps={{
            tooltip: {
              sx: {
                fontSize: '0.9rem',
                fontWeight: 'normal',
                backgroundColor: 'white',
                color: 'black',
                padding: '10px',
                boxShadow: 'rgb(0 0 0 / 20%) 0px 5px 5px -3px, rgb(0 0 0 / 14%) 0px 8px 10px 1px, rgb(0 0 0 / 12%) 0px 3px 14px 2px;',
              },
            },
          }}
        >
          <HelpOutlineIcon style={{ height: '1rem' }} />
        </Tooltip>
      </Stack>
    </FormControl>
  </Box>
);

ContentTypeButtons.propTypes = {
  contentType: PropTypes.string.isRequired,
  setContentType: PropTypes.func.isRequired,
};

export default ContentTypeButtons;
