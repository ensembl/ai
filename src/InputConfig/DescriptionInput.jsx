import React from 'react';

import PropTypes from 'prop-types';

import {
  TextField, FormControl, FormLabel,
} from '@mui/material';

import { displayData } from '../utils';

const DescriptionInput = ({ description, onDescriptionChange, contentType }) => (
  <FormControl>
    <FormLabel>{displayData[contentType].label2}</FormLabel>
    <TextField
      style={{ width: '54vw' }}
      size="small"
      InputProps={{ style: { fontSize: '0.85rem' } }}
      InputLabelProps={{ style: { fontSize: '0.85rem' } }}
      value={description}
      onChange={onDescriptionChange}
      placeholder={displayData[contentType].placeholder.description}
    />
  </FormControl>
);

DescriptionInput.propTypes = {
  description: PropTypes.string.isRequired,
  onDescriptionChange: PropTypes.func.isRequired,
  contentType: PropTypes.string.isRequired,
};

export default DescriptionInput;
