import React from 'react';

import PropTypes from 'prop-types';

import {
  Box, Stack, FormControl, FormLabel, ToggleButtonGroup, ToggleButton,
} from '@mui/material';

import { tones } from '../utils';

const ToneButtons = ({ tone, setTone }) => (
  <Box>
    <FormControl>
      <FormLabel id="content-type">Tone</FormLabel>
      <Stack direction="row" alignItems="center">
        <ToggleButtonGroup
          value={tone}
          exclusive
          onChange={(e) => setTone(e.target.value)}
        >
          {
            tones.map((data) => (
              <ToggleButton
                key={data.value}
                value={data.value}
                size="small"
                color="primary"
                style={{
                  fontSize: '0.7rem', padding: '7px', textTransform: 'none', width: '7rem',
                }}
              >
                {data.label}
              </ToggleButton>
            ))
          }

        </ToggleButtonGroup>
      </Stack>
    </FormControl>
  </Box>
);

ToneButtons.propTypes = {
  tone: PropTypes.string.isRequired,
  setTone: PropTypes.func.isRequired,
};

export default ToneButtons;
