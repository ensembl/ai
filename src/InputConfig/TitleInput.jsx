import React from 'react';

import PropTypes from 'prop-types';

import {
  TextField, FormControl, FormLabel,
} from '@mui/material';

import { displayData } from '../utils';

const TitleInput = ({ title, onTitleChange, contentType }) => (
  <FormControl>
    <FormLabel>Title</FormLabel>
    <TextField
      style={{ width: '30vw' }}
      InputProps={{ style: { fontSize: '0.85rem' } }}
      InputLabelProps={{ style: { fontSize: '0.85rem' } }}
      size="small"
      value={title}
      onChange={onTitleChange}
      placeholder={displayData[contentType].placeholder.title}
    />
  </FormControl>
);

TitleInput.propTypes = {
  title: PropTypes.string.isRequired,
  onTitleChange: PropTypes.func.isRequired,
  contentType: PropTypes.string.isRequired,
};

export default TitleInput;
