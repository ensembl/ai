import React from 'react';

import PropTypes from 'prop-types';
import {
  Box, Chip, CircularProgress, Stack,
} from '@mui/material';

import AddIcon from '@mui/icons-material/Add';

const TitleTags = ({
  addKeyword, tags, tagsStatus,
}) => (
  <Stack
    direction="row"
    alignItems="center"
    style={{ marginTop: '10px', marginBottom: '15px', height: '1.4rem' }}
  >
    <div style={{ fontSize: '0.7rem', marginRight: '5px' }}>Ideas: </div>
    {tagsStatus === 'fetching'
      && (
        <Stack direction="row" alignItems="center">
          <div style={{ fontSize: '0.7rem' }}>Generating some ideas...</div>
          <CircularProgress size="1rem" />
        </Stack>
      )}
    {tagsStatus !== 'fetching'
      && (
        <Box>
          {tags.map((tag) => (
            <Chip
              key={tag}
              style={{ height: '1.3rem', marginRight: '5px', fontSize: '0.65rem' }}
              label={tag}
              deleteIcon={<AddIcon style={{ height: '.9rem' }} />}
              onDelete={() => { addKeyword(tag, 'idea'); }}
            />
          ))}
        </Box>
      )}
  </Stack>
);

TitleTags.propTypes = {
  addKeyword: PropTypes.func.isRequired,
  tags: PropTypes.arrayOf(PropTypes.string).isRequired,
  tagsStatus: PropTypes.string.isRequired,
};

export default TitleTags;
