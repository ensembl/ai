import React from 'react';

import {
  BrowserRouter as Router,
  Routes, Route,
} from 'react-router-dom';
import Login from './Login';
import Home from './Home';

import { AuthProvider } from './AuthProvider';

const App = () => (
  <Router>
    <AuthProvider>
      <Routes>
        <Route exact path="/login" element={<Login />} />
        <Route exact path="/" element={<Home />} />
      </Routes>
    </AuthProvider>
  </Router>

);

export default App;
