import React, { useState, useEffect } from 'react';

import { useNavigate } from 'react-router-dom';
import PropTypes from 'prop-types';

import { Box, CircularProgress } from '@mui/material';

import { httpsCallable } from 'firebase/functions';
import { auth, functions } from './firebase-config';

import { setHeapProperties, resetHeapProperties } from './heapUtils';

export const AuthContext = React.createContext();

export const AuthProvider = ({ children }) => {
  const [loading, setLoading] = useState(true);
  const [currentUser, setCurrentUser] = useState(null);
  const [isTester, setIsTester] = useState(null);

  const navigate = useNavigate();
  const callable = httpsCallable(functions, 'isTester-isTester');

  auth.onAuthStateChanged(async (user) => {
    if (!user
      && window.location.pathname !== '/login'
    ) {
      navigate('/login');
      setCurrentUser(null);
    } else if (user) {
      setCurrentUser(user);
    } else {
      setLoading(false);
    }
  });

  useEffect(async () => {
    if (currentUser && isTester === null) {
      const tokenResult = await auth.currentUser.getIdTokenResult();
      if (tokenResult.claims.role === 'tester') {
        setLoading(false);
      } else {
        // Remove this code if custom claim is ready
        const res = await callable();
        const { isTester: isTesterRes } = res.data;
        setIsTester(isTesterRes);
        setHeapProperties(currentUser.uid, currentUser.email);
        if (!isTesterRes) {
          setCurrentUser(null);
          auth.signOut();
          resetHeapProperties();
        }
        setLoading(false);
      }
    }
  }, [currentUser]);

  return (
    <>
      {loading && (
        <Box display="flex" justifyContent="center" marginTop="30px">
          <span>Loading...</span>
          <CircularProgress size="1rem" />
        </Box>
      )}
      {!loading && (
        <AuthContext.Provider
          value={currentUser}
        >
          {children}
        </AuthContext.Provider>
      )}
    </>
  );
};

AuthProvider.propTypes = {
  children: PropTypes.node.isRequired,
};

export default AuthProvider;
