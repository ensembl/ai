import React, { useState, useRef } from 'react';

import { useNavigate } from 'react-router-dom';

import {
  Button, Box, Divider, Stack,
} from '@mui/material';
import ContentCopyIcon from '@mui/icons-material/ContentCopy';

import ReactQuill from 'react-quill';

import Issue from './Issue';

import { auth } from './firebase-config';

import { resetHeapProperties } from './heapUtils';

import 'react-quill/dist/quill.snow.css';
import './Home.css';

const Home = () => {
  const localStorageKey = `app.ensembl.ai:${auth.currentUser.email}`;
  const [content, setContent] = useState(window.localStorage.getItem(localStorageKey) || '');
  const navigate = useNavigate();
  const quillRef = useRef();

  const onSignOut = () => {
    auth.signOut();
    navigate('/login');
    resetHeapProperties();
  };

  const focusEditor = () => {
    setTimeout(() => {
      quillRef.current.getEditor().setSelection(quillRef.current.getEditor().getLength());
    }, 0);
  };

  const onQuillChange = (c) => {
    setContent(c);
    window.localStorage.setItem(localStorageKey, c);
  };

  const copyText = () => {
    const plainText = quillRef.current.getEditor().getText();
    navigator.clipboard.writeText(plainText);
  };

  return (
    <Stack direction="row" height="100%">
      <Box flexBasis="55%" style={{ padding: '0 15px' }} flexGrow={1} minHeight="100vh">
        <Stack direction="row" justifyContent="space-between">
          <h3>Creative business writing with AI 🦾</h3>
          <Box display="flex" alignItems="center">
            <Button
              variant="text"
              size="small"
              onClick={onSignOut}
              style={{ fontSize: '0.7rem' }}
            >
              Sign out
            </Button>
          </Box>
        </Stack>
        <Issue content={content} setContent={setContent} focusEditor={focusEditor} />
      </Box>
      <Divider
        orientation="vertical"
        variant="middle"
        flexItem
        style={{ margin: '0 10px' }}
      />
      <Box flexBasis="35%" flexGrow={1}>
        <Box position="fixed" padding="10px">
          <span style={{
            position: 'fixed',
            color: 'green',
            right: '40px',
            top: '21px',
            fontSize: '0.5rem',
          }}
          >
            Autosave ✅
          </span>
          <ReactQuill
            value={content}
            onChange={onQuillChange}
            ref={quillRef}
            style={{ height: '88vh', minWidth: '36vw' }}
          />
        </Box>
      </Box>
      <Button
        style={{
          zIndex: '10',
          position: 'fixed',
          right: '15px',
          bottom: '15px',
          height: '1.6rem',
          fontSize: '.7rem',
        }}
        variant="contained"
        color="info"
        onClick={copyText}
      >
        <Box display="flex" alignItems="center">
          <span>Copy plain text</span>
          <ContentCopyIcon style={{ height: '.8rem' }} />
        </Box>
      </Button>
    </Stack>
  );
};

export default Home;
