const setHeapProperties = (userId, email) => {
  window.heap.identify(userId);
  window.heap.addUserProperties({
    email,
  });
};

const resetHeapProperties = () => {
  window.heap.resetIdentity();
};

export { setHeapProperties, resetHeapProperties };
