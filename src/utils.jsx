import React from 'react';

const contentTypeTooltip = (
  <div>
    <p>
      <b>Proposed work</b>
      : Use it for a project or feature in a roadmap plan,
      business project plan or requirements doc.
      Use it as the background/motivation section in a user story.
    </p>
    <p>
      <b>Completed work</b>
      : Use it for a completed project of feature in a stakeholder report,
      sprint review, or internal release notes.
    </p>
    <p>
      <b>Status update</b>
      : Provide an update for a project or feature in progress.
    </p>
    <p>
      <b>Other</b>
      : Ensembl will create something related and interesting.
    </p>
  </div>
);

const displayData = {
  pitch: {
    value: 'pitch',
    label: 'Proposed work',
    label2: 'What’s the business benefit? Keep it brief',
    placeholder: { title: 'e.g. iOS mobile native app', description: 'e.g. Lots of members have been asking for this' },
  },
  report: {
    value: 'report',
    label: 'Completed work',
    label2: 'What’s the business benefit now that’s done? Keep it brief',
    placeholder: { title: 'e.g. Self-service refund request', description: 'e.g. Cut inbound tickets by 15%' },
  },
  progress: {
    value: 'progress',
    label: 'Status update',
    label2: 'What’s changed since the last update? Keep it brief',
    placeholder: { title: 'e.g. Login and password reset flow', description: 'e.g. Is on track for this iteration' },
  },
  other: {
    value: 'other',
    label: 'Other',
    label2: 'What’s it about?',
    placeholder: { title: 'e.g. Finalized staff onboarding process', description: 'e.g. Make onbarding smoothing and more standardized' },
  },
};

const containBadWords = (text) => {
  const badWordsRegex = /fuck|fucking|shit|asshole|bitch|cunt/;
  return badWordsRegex.test(text.toLowerCase());
};

const getLoadingMessage = (type) => {
  let display;
  switch (type) {
    case 'pitch':
      display = 'pitches';
      break;
    case 'progress':
      display = 'progress updates';
      break;
    case 'report':
      display = 'reports';
      break;
    default:
      display = 'paragraphs';
      break;
  }
  const messages = [
    `Interesting topic... writing ✍️ some ${display}...`,
    `Our robot 🤖 is working hard to come up with some ${display}...`,
    `Generating some examples of ${display} for you...`,
    'I\'ve got some 💡 ideas, writing them down now for you...',
  ];
  return messages[Math.floor(Math.random() * messages.length)];
};

const progressTags = ['On track', 'Delayed', 'Blocked', 'Postponed', 'Done'];

const tones = [
  { label: 'Formal', value: 'formal' },
  { label: 'Casual', value: 'casual' }];

export {
  displayData, containBadWords, getLoadingMessage, contentTypeTooltip,
  progressTags, tones,
};
