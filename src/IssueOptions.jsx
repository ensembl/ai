import React from 'react';

import PropTypes from 'prop-types';

import {
  Box, Button, Stack, TextField, Chip,
} from '@mui/material';
import AddIcon from '@mui/icons-material/Add';
import ArrowRightIcon from '@mui/icons-material/ArrowRight';

const IssueOptions = ({
  options, onOptionTextChange, onCopyOver, addKeyword,
}) => (
  Object.entries(options).map(([optionKey, value]) => (
    <Box key={`${optionKey}`} margin="20px 0">
      <Stack
        direction="row"
        justifyContent="space-between"
        alignItems="space-between"
      >
        <TextField
          style={{ width: '43vw' }}
          multiline
          required
          value={value.text}
          onChange={
            (e) => onOptionTextChange(
              optionKey,
              e.target.value,
            )
          }
          sx={
            { '& .MuiOutlinedInput-input': { fontSize: '0.8rem' } }
          }
        />
        <Box
          display="flex"
          marginLeft="10px"
          alignItems="center"
        >
          <Button
            size="small"
            variant="outlined"
            style={{ height: '1.5rem' }}
            onClick={(e) => onCopyOver(e, optionKey)}
          >
            <ArrowRightIcon style={{ height: '1.3rem' }} />
          </Button>
        </Box>
      </Stack>
      <Box marginTop="6px" fontSize="0.65rem">
        <span style={{ marginLeft: '2px' }}>Keywords:</span>
        {value.keywords.map((keyword) => (
          <Box key={keyword} component="span" marginLeft="5px">
            <Chip
              style={{ height: '1.3rem', fontSize: '0.65rem' }}
              label={keyword}
              deleteIcon={<AddIcon style={{ height: '.9rem' }} />}
              onDelete={() => { addKeyword(keyword, 'keyword'); }}
            />
          </Box>
        ))}
      </Box>
    </Box>
  ))
);

IssueOptions.propTypes = {
  options: PropTypes.arrayOf(
    PropTypes.objectOf(PropTypes.any),
  ).isRequired,
  onOptionTextChange: PropTypes.func.isRequired,
  onCopyOver: PropTypes.func.isRequired,
  addKeyword: PropTypes.func.isRequired,
};

export default IssueOptions;
