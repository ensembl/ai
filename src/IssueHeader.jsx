import React, { useState, useEffect } from 'react';

import PropTypes from 'prop-types';

import { httpsCallable } from 'firebase/functions';

import {
  Alert, Box, Button, Stack, CircularProgress,
} from '@mui/material';

import ContentTypeButtons from './InputConfig/ContentTypeButtons';

import { functions } from './firebase-config';

import { containBadWords, getLoadingMessage, progressTags } from './utils';
import TitleTags from './InputConfig/TitleTags';
import ToneButtons from './InputConfig/ToneButtons';
import TitleInput from './InputConfig/TitleInput';
import DescriptionInput from './InputConfig/DescriptionInput';

const IssueHeader = ({
  title, setTitle, description, setDescription, appState, setAppState,
  setOptions, addKeyword, tags, setTags,
}) => {
  const [contentType, setContentType] = useState('pitch');
  const [hasInappropriateContent, setHasInappropriateContent] = useState(false);
  const [hasLongInput, setHasLongInput] = useState(false);
  const [tone, setTone] = useState('formal');
  const [tagsStatus, setTagsStatus] = useState('waiting');

  const onGenerateOptions = async () => {
    setAppState('loading');
    const callable = httpsCallable(functions, 'openAICompletionChoices-openAICompletionChoices');
    const res = await callable({
      title,
      description,
      n: 3,
      type: contentType,
      tone,
      endpoint: 'davinci',
    });
    const { data } = res;
    const { error, choices } = data;
    if (error) {
      setAppState('error');
    } else {
      setOptions(choices);
      setAppState('completed');
    }
  };

  const generateTags = async () => {
    if (contentType !== 'progress') {
      setTagsStatus('fetching');
      const callable = httpsCallable(functions, 'openAIGenerateTags-openAIGenerateTags');
      const res = await callable({ title });
      const { data } = res;
      const { tags: tagsRes } = data;
      setTags(tagsRes);
      setTagsStatus('waiting');
    }
  };

  const onTitleChange = (e) => {
    setHasInappropriateContent(containBadWords(e.target.value));
    setHasLongInput(e.target.value.length > 200);
    setTitle(e.target.value);
  };

  const onDescriptionChange = (e) => {
    setHasInappropriateContent(containBadWords(e.target.value));
    setHasLongInput(e.target.value.length > 200);
    setDescription(e.target.value);
  };

  const onClearAll = () => {
    setTitle('');
    setDescription('');
    setOptions({});
    setAppState('writing');
    setTags([]);
  };

  useEffect(() => {
    let timeoutId;
    if (title !== '') {
      timeoutId = setTimeout(generateTags, 1000); // https://stackoverflow.com/questions/53071774/reactjs-delay-onchange-while-typing
    }
    return () => clearTimeout(timeoutId);
  }, [title]);

  useEffect(() => {
    if (contentType !== 'progress') {
      setTags([]);
    } else {
      setTags(progressTags);
    }
  }, [contentType]);

  return (
    <>
      <Stack direction="row" gap="6px" width="100%" flexWrap="wrap" justifyContent="space-between">
        <Box>
          <ContentTypeButtons contentType={contentType} setContentType={setContentType} />
        </Box>
        <Box marginRight="5%">
          <ToneButtons tone={tone} setTone={setTone} />
        </Box>

      </Stack>
      <Box marginTop="6px">
        <TitleInput
          title={title}
          onTitleChange={onTitleChange}
          contentType={contentType}
        />
      </Box>
      <Box marginTop="6px">
        <DescriptionInput
          description={description}
          onDescriptionChange={onDescriptionChange}
          contentType={contentType}
        />
        <TitleTags
          addKeyword={addKeyword}
          tags={tags}
          tagsStatus={tagsStatus}
        />
        {hasInappropriateContent && (
          <p style={{ fontSize: '0.8rem', color: 'red' }}>
            Input may contain inappropriate content,
            please change it before trying to generate an output.
          </p>
        )}
        {hasLongInput && (
          <p style={{ fontSize: '0.8rem', color: 'red' }}>
            Input must be less than 200 characters in length.
          </p>
        )}
        {appState === 'error' && (
          <p style={{ fontSize: '0.8rem', color: 'red' }}>
            Something went wrong with our AI, please try again.
          </p>
        )}
        <Stack marginTop="10px" direction="row" justifyContent="space-between">
          <Button
            onClick={onGenerateOptions}
            variant="contained"
            size="small"
            style={{ height: '2rem' }}
            disabled={appState === 'loading'
              || (!title || !description)
              || hasInappropriateContent || hasLongInput}
          >
            Generate with AI 🤖
          </Button>
          <Button
            onClick={onClearAll}
            size="small"
          >
            Clear all
          </Button>
        </Stack>
        {appState === 'loading' && (
          <Box marginTop="10px">
            <Alert icon={false} severity="info">
              <Stack direction="row" alignItems="center">
                <Box marginRight="10px">{getLoadingMessage(contentType)}</Box>
                <CircularProgress color="inherit" size="1rem" />
              </Stack>
            </Alert>
          </Box>
        )}
      </Box>
    </>
  );
};

IssueHeader.propTypes = {
  title: PropTypes.string.isRequired,
  setTitle: PropTypes.func.isRequired,
  description: PropTypes.string.isRequired,
  setDescription: PropTypes.func.isRequired,
  appState: PropTypes.string.isRequired,
  setAppState: PropTypes.func.isRequired,
  setOptions: PropTypes.func.isRequired,
  addKeyword: PropTypes.func.isRequired,
  tags: PropTypes.arrayOf(PropTypes.string).isRequired,
  setTags: PropTypes.func.isRequired,
};

export default IssueHeader;
