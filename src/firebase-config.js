// Import the functions you need from the SDKs you need
import { initializeApp } from 'firebase/app';

// Uncomment to test in emulator
import { getFunctions, connectFunctionsEmulator } from 'firebase/functions';
import { getAuth, connectAuthEmulator } from 'firebase/auth';
import { getFirestore, connectFirestoreEmulator } from 'firebase/firestore';

const firebaseConfig = {
  apiKey: 'AIzaSyDGhGHSqmk4O0cM2xAM2uAah0T4pw_vGxk',
  authDomain: 'auth.app.ensembl.ai',
  projectId: 'ensembl-ai',
  storageBucket: 'ensembl-ai.appspot.com',
  messagingSenderId: '701004292349',
  appId: '1:701004292349:web:c2db238a41344e39995e94',
  measurementId: 'G-NRX4LQK4F0',
};

const app = initializeApp(firebaseConfig);
const auth = getAuth(app);
const functions = getFunctions(app);
const db = getFirestore(app);

// Uncomment to test in emulator
if (window.location.hostname === 'localhost') {
  connectFunctionsEmulator(functions, 'localhost', 5001);
  connectAuthEmulator(auth, 'http://localhost:9099');
  connectFirestoreEmulator(db, 'localhost', 8080);
}

export {
  app, auth, functions, db,
};
