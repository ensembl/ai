// One time script to update custom claim

const { admin } = require("./admin");

const auth = admin.auth();

const listUsers = async () => {
  const users = await auth.listUsers();

  users.users.forEach((user) => {
    auth.setCustomUserClaims(user.uid, { role: "tester" });
  });
};

listUsers();
