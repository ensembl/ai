const functions = require("firebase-functions");

const { logger } = functions;

const { admin } = require("./admin");

const firestore = admin.firestore();

const {
  openAiCall, getConfig, getContentFilterConfig, getKeywordConfig,
} = require("./utils");

const isEmulator = process.env.FIREBASE_AUTH_EMULATOR_HOST === "localhost:9099";
const testingUid = "testing";

const replaceUrl = (string) => {
  const regex = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_+.~#?&//=]*)/;
  return string.replace(regex, "<url here>");
};

const removeExtraParagraphs = (string) => string.split("\n")[0].split("  ")[0];
const capitalizeFirstLetter = (string) => string.charAt(0).toUpperCase() + string.slice(1);

const getCleanResponse = (string) => removeExtraParagraphs(replaceUrl(string)).trim();

const setHistory = async ({
  uid, title, description, type, choices,
}) => {
  await firestore.collection(`users/${uid}/history`).add({
    title, description, type, choices, createdAt: new Date(),
  });
};

const getKeywords = async ({ prompt, uid }) => {
  const res = await openAiCall({
    data: getKeywordConfig({ prompt, uid }),
    endpoint: "davinci",
  });

  const { text } = res.data.choices[0];
  const choices = text.split("\n")[0].split(",")
    .map((choice) => capitalizeFirstLetter(choice.trim()))
    .filter((choice) => choice !== "");
  return choices;
};

exports.openAICompletionChoices = functions.https.onCall(async (data, context) => {
  logger.info("openAICompletionChoices fired.");
  if (!context.auth) {
    functions.logger.error("openAICompletionChoices called without auth");
  }

  const { uid } = context.auth;
  const {
    title,
    description,
    type,
    tone = "formal",
    n,
    endpoint,
  } = data;

  try {
    const res = await openAiCall(
      {
        data: getConfig(
          {
            title,
            description,
            n,
            type,
            tone,
            uid: isEmulator ? testingUid : uid,
          },
        ),
        endpoint,
      },
    );

    const { choices } = res.data;

    const safeChoices = [];
    await Promise.all(
      choices.map(async (choice) => {
        const cleanResponse = getCleanResponse(choice.text);
        const result = await openAiCall(
          {
            data: getContentFilterConfig({
              text: cleanResponse,
              uid: isEmulator ? testingUid : uid,
            }),
            endpoint: "content-filter-alpha",
          },
        );
        const label = result.data.choices[0].text;
        if (label === "0" || label === "1") {
          const keywords = await getKeywords(
            { prompt: cleanResponse, uid: isEmulator ? testingUid : uid },
          );
          safeChoices.push({ text: cleanResponse, keywords });
        } else {
          safeChoices.push({ text: "This response did not pass our safety guideline, please change your prompt and try again" });
        }
      }),
    );
    setHistory({
      uid, title, description, type, choices: safeChoices,
    });
    return { choices: safeChoices };
  } catch (error) {
    return { error };
  }
});
