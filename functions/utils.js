const axios = require("axios");
const functions = require("firebase-functions");

const OPENAI_KEY = functions.config().openai.key;

exports.openAiCall = async ({ data, endpoint }) => {
  const response = await axios({
    url: `https://api.openai.com/v1/engines/${endpoint}/completions`,
    headers: {
      Authorization: `Bearer ${OPENAI_KEY}`,
      "Content-Type": "application/json",
    },
    data,
    method: "post",
  });
  return response;
};

const getPrompt = ({ type, title, description }) => {
  switch (type) {
    case "pitch":
      return `
        This is a business report writer:
        ###
        User story: Streamline navigation UI
        Description: Will fix user confusion
        Four-sentence report: Users are consistently reporting that the navigation menu is confusing. This change will simplify the navigation design and help users find exactly what they need when they are on our website. We think users will be delighted with the improved design and come back to it again and again. It will also improve the number of inbound leads.
        ###
        User story: Android mobile app
        Description: Customers have been requesting this
        Four-sentence report: Customers on Android have been requesting a native mobile app. This will also increase traffic by 20% (per our user research) with a new inbound channel via the Google Play Store. It will bring parity to our mobile strategy, joining the already shipped iOS app. We think it's critical to have a coherent mobile strategy today.
        ###
        User story: Recurring investment flow
        Description: Will encourage more usage and raise gross investments
        Four-sentence report: Recurring investments is a strategic feature to get customers to put more money into our ecosystem, ultimately growing usage. It will also have an impact in raising total investments on the platform. It will give a reason for users to continually return to our product. Our competitors already have this feature and this will help us catch up accordingly.
        ###
        User story: ${title}
        Description: ${description}
        Four-sentence report:`;
    case "progress":
      return `
        This is a business progress update writer:
        ###
        User story: MySQL migration
        Description: Delayed because of vendor security bug
        Four-sentence report: The MySQL migration work is delayed because the team discovered a security bug in the latest version of MySQL (which we are using). The db folks will ship a workaround first before the main migration is done. And they will thoroughly test with the business team too. We're not sure when the work will be done, but the db team has promised an update by the end of this week.
        ###
        User story: 2FA flow
        Description: On track for this sprint
        Four-sentence report: The 2FA flow issue is on track to be finished for this sprint. The customer support team has already been briefed on the new feature and is expecting it to be done when the sprint is complete. They've already started UAT testing so we can parallelize our efforts and speed up shipping the feature. So we don't anticipate any additional delays.
        ###
        User story: Dark mode for web and mobile
        Description: Ahead of schedule and will be completed this month
        Four-sentence report: Dark mode for web and mobile will be completed this month. It’s ahead of schedule since the frontend team hired and onboarded two new folks in the past week. The team isn't slowing down because of this. They are already doing research on the next feature of offering more theme customization options.
        ###
        User story: Share to Facebook
        Description: Delayed because of Facebook platform change
        Four-sentence report: The share to Facebook feature is delayed for at least two weeks. Facebook recently changed their API and the team needs to get new developer credentials approved on the Facebook side. We've been in contact with the Facebook developer relations department to make sure there's no more hiccups here. Once we get confirmation I'll provide another update in this channel.
        ###
        User story: ${title}
        Description: ${description}
        Four-sentence report:`;
    case "report":
      return `
        This is a business report writer:
        ###
        User story: Track my order
        Description: Was a frequent user-requested feature per customer success team
        Four-sentence report: The customer success team said track my order was a highly requested feature. We worked with them to ship an MVP last week and so far they’ve been reporting significant positive feedback from customers in the past 3 days. We've also received 10 new positive iOS app reviews as a result of this change in the App Store. If we don't hear any major issues we'll be working on the parity Android feature in the next sprint.
        ###
        User story: Design updates to landing page
        Description: Improvement to marketing and our brand
        Four-sentence report: We finished the design updates to the landing page this sprint. We worked closely with the marketing team on it and they think it will significantly improve our inbound funnel metrics. The brand marketing team also quickly did some UAT. We've hooked up Tableau and will see the metrics being piped into Slack as users experience the new landing page.
        ###
        User story: Simplifying menu UI
        Description: Fixed user confusion
        Four-sentence report: The old menu UI was causing a lot of confusion. Based on several user research sessions, the new version is a lot easier to use and will reduce a lot of user pain, especially reducing user support requests. We expect the average time people stay our website to increase as a result. We also did a round of internal testing with the customer support folks, squashing a bunch of bugs before release.
        ###
        User story: ${title}
        Description: ${description}
        Four-sentence report:`;
    default:
      return `
        This is a business report writer:
        ###
        User story: Paypal payment integration
        Description: Was a critical feature this quarter
        Four-sentence report: The Paypal payment integration feature was finished this sprint. It was an important feature this quarter to further our vision of reducing friction in our payments flow by offering more options for customers. We'll be monitoring metrics to validate this and work with customer support in fixing any issues that may arise. If we don't see any issues, we'll follow up quickly with the Stripe integration work.
        ###
        User story: Mobile web refactor
        Description: Modernized original web app shipped 10 years ago
        Four-sentence report: Our original web app experience launched 10 years ago when our customers were 95% on desktop. Shipping this mobile web refactor project finally brings our experience inline with our users (who are 99% on mobile) and catches up to our more recent competitors. It also dovetails with our native app strategy, pushing customers to the App Store app when they are on our website on their phone. We'll keep track of desktop vs mobile usage breakdown going forward.
        ###
        User story: Access my viewing history
        Description: Customers have been wanting more transparency and fine-grained control
        Four-sentence report: Customers want more transparency in how we’re collecting their viewing habits. They want more control of their own account and settings. This is a first feature in our broader strategy to help users feel more in control and for us to gain more trust back from them. The next set of features in this vein will be worked on in Q3 later this year.
        ###
        User story: ${title}
        Description: ${description}
        Four-sentence report:`;
  }
};

const getCausualPrompt = ({ type, title, description }) => {
  switch (type) {
    case "pitch":
      return `
        This is a business report writer:
        ###
        User story: Streamline navigation UI
        Description: Will fix user confusion
        Four-sentence report: The nav menu is causing a lot of headaches with folks. So this change will simplify the nav design and help users when they are on our site. We think this will really wow and delight our users, bringing them back again and again. And as a result we think inbound leads will go up.
        ###
        User story: Android mobile app
        Description: Customers have been requesting this
        Four-sentence report: Android folks have been banging the drum for their own app. We did some research and this will likely bring more users via the Google Play Store. The Android app will join our iOS app together. And we think it will be a pretty good overall mobile strategy together.
        ###
        User story: Recurring investment flow
        Description: Will encourage more usage and raise gross investments
        Four-sentence report: Recurring investments will be an interesting change to bring more users. It will help bring more investments to our platform. More people will come back to use our app. This will help us catch up with our competitors too.
        ###
        User story: ${title}
        Description: ${description}
        Four-sentence report:`;
    case "progress":
      return `
        This is a business progress update writer:
        ###
        User story: MySQL migration
        Description: Delayed because of vendor security bug
        Four-sentence report: The MySQL migration work got held up since the team found a security bug in the latest MySQL version. The db folks will ship a workaround first before finishing the main migration work. They’re going to get together with the business folks for UAT too. We’re not really sure when the work is going to be finished, but the db folks said they’ll update us by the end of week.
        ###
        User story: 2FA flow
        Description: On track for this sprint
        Four-sentence report: The 2FA flow work is on track for this sprint. The customer support folks have already reviewed it and are quite excited. They’re about halfway through UAT at the moment. Fingers-crossed, we don’t think there’s going to be any hold ups.
        ###
        User story: Dark mode for web and mobile
        Description: Ahead of schedule and will be completed this month
        Four-sentence report: Dark mode for web and mobile is wrapping up this month. It’s actually ahead of schedule since the frontend folks onboarded two new team members earlier this week. Yay! But they’re not slowing down at all. They’re already researching the next feature of giving users more choice.
        ###
        User story: Share to Facebook
        Description: Delayed because of Facebook platform change
        Four-sentence report: The share to Facebook piece is held up for another two weeks. Bummer. Facebook actually changed their API recently and so the team has to get new dev credentials approved on the Facebook side. Thankfully we’ve been in close contact with dev rel over at Facebook HQ. So we’re somewhat sure we won’t get any more hiccups.
        ###
        User story: ${title}
        Description: ${description}
        Four-sentence report:`;
    case "report":
      return `
        This is a business report writer:
        ###
        User story: Track my order
        Description: Was a frequent user-requested feature per customer success team
        Four-sentence report: The customer success folks told us that track my order was something customers have been asking for a lot. We got together with them last week to get an MVP out the door. And now they’ve been getting lots of great positive feedback! iOS app reviews have been on fire as a result. We’re probably going to port this over to Android if everything goes well.
        ###
        User story: Design updates to landing page
        Description: Improvement to marketing and our brand
        Four-sentence report: The landing page designs were finished in this past sprint. We teamed up with the marketing team on it and they think it’s going to really juice those funnel metrics. The brand marketing side also already took it out for a spin. The metrics themselves are going to be zipped to Tableau and Slack, as users experience the new landing page.
        ###
        User story: Simplifying menu UI
        Description: Fixed user confusion
        Four-sentence report: The old menu UI was super painful for users. It was super confusing. The new version is now much easier to use and we think it’s going to ease up the support team’s load. This should also increase the time that users are actually hanging out on our website, which we think is great!
        ###
        User story: ${title}
        Description: ${description}
        Four-sentence report:`;
    default:
      return `
        This is a business report writer:
        ###
        User story: Paypal payment integration
        Description: Was a critical feature this quarter
        Four-sentence report: The Paypal integration feature wrapped up this sprint. It’s a big piece in pushing our vision forward of reducing friction in the payments flow more generally. We’re going to keep an eye on metrics over time to validate this. If all goes well, we’ll do a fast follow with the upcoming Stripe integration work.
        ###
        User story: Mobile web refactor
        Description: Modernized original web app shipped 10 years ago
        Four-sentence report: Our original web app came out 10 years ago. Back then everyone was mostly on the desktop. So shipping this mobile refactor is huge. It actually meets our users’ expectations, who really spend all their time on mobile. It also actually jives with the native app itself, which we shipped a while back. We’ll keep track of desktop and mobile usage over time too just to make sure everything is kosher.
        ###
        User story: Access my viewing history
        Description: Customers have been wanting more transparency and fine-grained control
        Four-sentence report: Customers are telling us that they want more visibility and transparency into how we’re monitoring them. They want more control of their own settings. So this is the first change in our broader strategy to help folks feel more in control. We want to win back trust from them. We’re going to continue this work in Q3.
        ###
        User story: ${title}
        Description: ${description}
        Four-sentence report:`;
  }
};

const getKeywordPrompt = ({ prompt }) => `
  This is a keyword identifier:
  ###
  Prompt: Users are consistently reporting that the navigation menu is confusing. This change will simplify the navigation design and help users find exactly what they need when they are on our website. We think users will be delighted with the improved design and come back to it again and again.
  Keywords: Navigation, Customer delight, website
  ###
  Prompt: The MySQL migration work is delayed because the team discovered a security bug in the latest version of MySQL (which we are using). The db folks will ship a workaround first before the main migration is done. We're not sure when the work will be done. But the db team has promised an update by the end of this week.
  Keywords: Tech debt, Migration, At risk
  ###
  Prompt: We finished the design updates to landing page work this sprint. We worked closely with the marketing team on it and they think it will significantly improve our inbound funnel metrics. We've hooked up Tableau and will see the metrics being piped into Slack as users experience the new landing page.
  Keywords: Marketing team, Metrics and analytics, Landing page
  ###
  Prompt: Customers want more transparency in how we’re collecting their viewing habits. This is a first feature in our broader strategy to help users feel more in control and for us to gain more trust back from them. The next set of features in this vein will be worked on in Q3 later this year.
  Keywords: Transparency and privacy, User trust, Roadmap plan
  ###
  Prompt: ${prompt}
  Keywords:`;

const getTagsPrompt = ({ prompt }) => `
  This is a tag identifier:
  ###
  Title: Paypal payment integration
  Tags: Payments, API integration, Critical feature, Backend team, Integration testing
  ###
  Title: Access my viewing history
  Tags: Privacy and security, User control, Customer support, User requested, Top priority
  ###
  Title: Streamline navigation UI
  Tags: UX design, Frontend changes, User delight, Retention, Improve conversion
  ###
  Title: Dark mode for web and mobile
  Tags: Dark mode, User customization, UAT, Strategy, Marketing
  ###
  Title: MySQL migration
  Tags: Tech debt, Backend, User data, Database, Security
  ###
  Title: ${prompt}
  Tag:`;

exports.getKeywordConfig = ({
  prompt, uid,
}) => ({
  prompt: getKeywordPrompt({ prompt }),
  max_tokens: 50,
  temperature: 0.7,
  top_p: 1,
  stop: "###",
  user: uid,
});

exports.getConfig = ({
  title, description, n, type, uid, tone,
}) => ({
  prompt: tone === "casual"
    ? getCausualPrompt({ type, title, description })
    : getPrompt({ type, title, description }),
  max_tokens: 130,
  temperature: 1,
  top_p: 0.4,
  presence_penalty: 2,
  frequency_penalty: 2,
  stop: "###",
  n,
  user: uid,
});

exports.getContentFilterConfig = ({
  text, uid,
}) => ({
  prompt: `<|endoftext|>${text}\n--\nLabel:`,
  temperature: 0,
  max_tokens: 1,
  top_p: 0,
  logprobs: 10,
  user: uid,
});

exports.getTagsConfig = ({
  title, uid,
}) => ({
  prompt: getTagsPrompt({ prompt: title }),
  max_tokens: 80,
  temperature: 0.5,
  top_p: 1,
  stop: "###",
  user: uid,
});
