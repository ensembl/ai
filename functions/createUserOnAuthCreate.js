const functions = require("firebase-functions");

const { logger } = functions;

const { admin } = require("./admin");

const auth = admin.auth();

const firestore = admin.firestore();

exports.createUserOnAuthCreate = functions.auth.user().onCreate(async (user) => {
  logger.info("createUserOnAuthCreate fired.");
  const {
    uid,
    email,
    displayName,
    photoURL,
  } = user;

  const snapshot = await firestore.collection("waitlist")
    .where("email", "==", email).get();

  if (!snapshot.empty) {
    await firestore.collection("users").doc(uid).set({
      email, displayName, photoURL, createdAt: new Date(),
    });
    auth.setCustomUserClaims(uid, { role: "tester" });
  } else {
    await auth.deleteUser(uid);
  }
});
