const admin = require("firebase-admin");

// Uncomment if running script on server
// const serviceAccount = require("./firebase-adminsdk-token.json");
// settings.credential = admin.credential.cert(serviceAccount);

const useEmulator = false;

const settings = {
  projectId: "ensembl-ai",
};

if (useEmulator) {
  process.env.FIRESTORE_EMULATOR_HOST = "localhost:8080";
  process.env.FIREBASE_AUTH_EMULATOR_HOST = "localhost:9099";
}

admin.initializeApp(settings);

module.exports = { admin };
