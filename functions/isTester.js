const functions = require("firebase-functions");

const { logger } = functions;

const { admin } = require("./admin");

const firestore = admin.firestore();

exports.isTester = functions.https.onCall(async (data, context) => {
  logger.info("isTester fired.");
  if (!context.auth) {
    functions.logger.error("isTester called without auth");
  }
  const { email } = context.auth.token;

  const snapshot = await firestore.collection("waitlist")
    .where("email", "==", email).get();

  return { isTester: !snapshot.empty };
});
