const functions = require("firebase-functions");

const { logger } = functions;

const {
  openAiCall, getTagsConfig,
} = require("./utils");

const isEmulator = process.env.FIREBASE_AUTH_EMULATOR_HOST === "localhost:9099";
const testingUid = "testing";

exports.openAIGenerateTags = functions.https.onCall(async (data, context) => {
  logger.info("openAIGenerateTags fired.");
  if (!context.auth) {
    functions.logger.error("openAIGenerateTags called without auth");
  }

  const { uid } = context.auth;
  const {
    title,
  } = data;

  const res = await openAiCall({
    data: getTagsConfig({
      title,
      uid: isEmulator ? testingUid : uid,
    }),
    endpoint: "davinci",
  });

  const choices = res.data.choices[0].text.split(",");

  const cleanChoices = choices
    .map((choice) => choice.trim())
    .filter((choice) => choice.length < 30)
    .slice(0, 6);

  return ({ tags: [...new Set(cleanChoices)] });
});
